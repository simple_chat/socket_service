package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

type Config struct {
	HttpPort string
	Broker   string
	Port     string
	ClientID string
	UserName string
	Password string
	Topic    string
}

func Load() Config {
	err := godotenv.Load(".env")
	if err != nil {
		log.Println("error read .env: ", err)
	}
	c := Config{
		HttpPort: os.Getenv("HTTP_PORT"),
		Broker:   os.Getenv("BROKER"),
		Port:     os.Getenv("PORT"),
		ClientID: os.Getenv("CLIENT_ID"),
		UserName: os.Getenv("USER_NAME"),
		Password: os.Getenv("PASSWORD"),
		Topic:    os.Getenv("TOPIC"),
	}
	return c
}
