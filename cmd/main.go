package main

import (
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/socket_service/config"
	"gitlab.com/socket_service/mqtt"
	"gitlab.com/socket_service/websocket"
)

func main() {
	router := gin.Default()
	cfg := config.Load()

	mqttHander := mqtt.NewMqqtt(cfg)

	err := mqttHander.MessageHandler()
	if err != nil {
		log.Println("Error messageHanler: ", err)
	}

	hub := websocket.NewHub()

	go hub.Run()

	router.GET("/ws", func(c *gin.Context) {
		websocket.ServeWs(hub, c)
	})

	err = mqttHander.Publisher("Connection websocket!!!!")
	if err != nil {
		log.Println("Error publish: ", err)
	}
	router.Run(cfg.HttpPort)
}
